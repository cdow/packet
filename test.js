#!/usr/local/bin/node

var packet = require('./build/Release/packet');

var statusPacket = packet.status();
console.log("status", statusPacket);

var unlockPacket = packet.unlock();
console.log("unlock", unlockPacket);

var lockPacket = packet.lock();
console.log("lock", lockPacket);
#include <node.h>
#include <node_buffer.h>
#include <v8.h>

extern "C" {
#include <stdint.h>
#include <string.h>
}

using namespace v8;
using namespace node;

// packet type
#define pcktCommand  0xC 
#define pcktResponse 0xA
#define pcktError    0xE
typedef uint8_t packetType_t;

// commands
#define cmdStatus  0
#define cmdUnlock  1
#define cmdLock    2
#define cmdSetTime 3
#define cmdGetTime 4
typedef unsigned char command_t;

typedef struct {
  packetType_t type;
  command_t    command;
  uint16_t     pad_reserved_0;
  union {
    struct {
      uint32_t arg0;
      uint32_t arg1;
    } args;
    struct {
      uint32_t epochTimeInS;
      uint32_t reserved;
    } time;
  };
} cmdPacket_t;


void makeCommandPacket(packetType_t type, command_t command, cmdPacket_t *packet) {
  memset((void *) packet, 0, sizeof(cmdPacket_t));
  packet->type = type;
  packet->command = command;
}

Handle<Value> status(const Arguments& args) {
  HandleScope scope;
  cmdPacket_t packet;

  makeCommandPacket(pcktCommand, cmdStatus, &packet);

  Local<Buffer>  pcktBuff = node::Buffer::New((char *) &packet, sizeof(packet));

  return scope.Close(pcktBuff->handle_);
  
}

Handle<Value> unlock(const Arguments& args) {
  HandleScope scope;
  cmdPacket_t packet;

  makeCommandPacket(pcktCommand, cmdUnlock, &packet);

  Local<Buffer> pcktBuff = node::Buffer::New((char *) &packet, sizeof(packet));

  return scope.Close(pcktBuff->handle_);

}

Handle<Value> lock(const Arguments& args) {
  HandleScope scope;
  cmdPacket_t packet;

  makeCommandPacket(pcktCommand, cmdLock, &packet);

  Local<Buffer> pcktBuff = node::Buffer::New((char *) & packet, sizeof(packet));

  return scope.Close(pcktBuff->handle_);
}

void init(Handle<Object> target) {
#define ADD_FUNCTION_TO_MODULE(name) \
  target->Set(String::NewSymbol((#name)), \
  FunctionTemplate::New((name))->GetFunction());

  ADD_FUNCTION_TO_MODULE(status);
  ADD_FUNCTION_TO_MODULE(unlock);
  ADD_FUNCTION_TO_MODULE(lock);
}

NODE_MODULE(packet, init)